# README #

VulnScan-Nessus is an adapter for the Nessus Vulnerability Scanner which implements the methods provided by the VulnScan-API. The basic features of VulnScan-Nessus are described in the Usage page.

## License ##
This script is licensed under the GNU General Public License in version 3. See http://www.gnu.org/licenses/ for further details.


## Usage ##
see the usage page on https://bitbucket.org/cbless/vulnscan-nessus/wiki/Usage