/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NessusClientReportTest {
    
    private final NessusClient client;
    
    private static String scanId;
    
    public NessusClientReportTest() throws Exception {
        client = new NessusClient(TestConfiguration.HOST, 
                TestConfiguration.PORT, true);
        client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
        scanId = TestConfiguration.EXISTING_SCAN_ID;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getScans method, of class NessusClient.
     */
    private void getScanId() {
        try{
            List<Scan> scans = client.getScans();
            assertTrue(scans.size() > 0 );
            scanId =  scans.get(0).getId();
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }

   
    @Test
    public void test02GetResults(){
        System.out.println("getResults");
        List<Vulnerability> vulns;
        try {
            System.out.println("ID: "+scanId);
            vulns = client.getResults(scanId);
            System.out.println(vulns);
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void test03GetResultsByHost(){
        System.out.println("getResultsByHost");
        try {
            Map<String, List<Vulnerability>> result = client.getResultsByHost(scanId);
            for (String k : result.keySet()){
                System.out.println(k + ": "+ result.get(k));
            }
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }
}
