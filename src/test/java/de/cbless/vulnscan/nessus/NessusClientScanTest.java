/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.NotFoundException;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NessusClientScanTest {
    
    private final NessusClient client;
    
    private static int scanListSize;
    private static String scanId;
    
    public NessusClientScanTest() throws Exception {
        client = new NessusClient(TestConfiguration.HOST, 
                TestConfiguration.PORT, true);
        client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getScans method, of class NessusClient.
     */
    @Test
    public void test03GetScans() {
        System.out.println("getScans");
        try{
            List<Scan> scans = client.getScans();
            assertTrue(scans.size() > 0 );
            scanListSize = scans.size();
            scanId = scans.get(0).getId();
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of getScan method, of class NessusClient.
     */
    @Test
    public void test04GetScan(){
        System.out.println("getScan");
        try{
            Scan scan = client.getScan(scanId);
            assertNotNull(scan.getId());
            assertTrue(scan.getId().equals(scanId));
        } catch(NotFoundException ex){
            fail("scan should be found");
        } catch (ClientException ex){
            fail(ex.getMessage());
        }
    }

    

    /**
     * Test of getScanDetails method, of class NessusClient.
     */
    @Test
    public void test06GetScanDetails() {
        System.out.println("getScanDetails");
        try{
            ScanDetails scan = client.getScanDetails(scanId);
            assertNotNull(scan.getId());
            assertTrue(scan.getId().equals(scanId));
        } catch(NotFoundException ex){
            fail("scan should be found");
        } catch (ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of launch method, of class NessusClient.
     */
    @Test
    public void test07Launch(){
        System.out.println("launch");
        try{
            String returnId = client.launch(scanId);
            assertTrue(scanId.equals(returnId));
        } catch (ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of pause method, of class NessusClient.
     */
    @Test
    public void test08Pause()  {
        System.out.println("pause");
        try{
            client.pause(scanId);
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of resume method, of class NessusClient.
     */
    @Test
    public void test09Resume() {
        System.out.println("resume");
        try{
            String returnId = client.resume(scanId);
            assertTrue(scanId.equals(returnId));
        } catch (ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of stop method, of class NessusClient.
     */
    @Test
    public void test10Stop() {
        System.out.println("stop");
        try{
            client.stop(scanId);
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }
    
    
}
