/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.SessionException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NessusClientSessionTests {
    
    private static NessusClient client;
    
    public NessusClientSessionTests() throws Exception {
        client = new NessusClient(TestConfiguration.HOST, 
                TestConfiguration.PORT, true);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class NessusClient.
     */
    @org.junit.Test
    public void test01failedLogin() {
        System.out.println("login");
        try{
            client.login(TestConfiguration.UNKNOWN_USER, 
                    TestConfiguration.PASSWORD);
        } catch(SessionException ex){
            if (client.isAuthenticated()){
                fail("isAuthenticated should return false");
            }
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of login method, of class NessusClient.
     */
    @org.junit.Test
    public void test02SuccessfullLogin() {
        System.out.println("login");
        try{
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            if (!client.isAuthenticated()){
                fail("isAuthenticated should return true");
            }
        } catch(SessionException ex){
            fail(ex.getMessage());
        } catch(ClientException ex){
            fail(ex.getMessage());
        }
    }

}
