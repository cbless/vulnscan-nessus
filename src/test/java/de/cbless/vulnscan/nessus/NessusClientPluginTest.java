/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.NotFoundException;
import de.cbless.vulnscan.api.model.PluginFamily;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NessusClientPluginTest {
    
    private final NessusClient client;
    
    private static String familyId;
    
    public NessusClientPluginTest() throws Exception {
        client = new NessusClient(TestConfiguration.HOST, 
                TestConfiguration.PORT, true);
        client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        List<PluginFamily> families;
        try {
            families = client.listFamilies();
            if (families.size() > 0){
                familyId = families.get(0).getId();
            }
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testListFamilies(){
        System.out.println("listFamilies");
        try {
            List<PluginFamily> families = client.listFamilies();
            for (PluginFamily f : families){
                assertNotNull(f.getId());
                assertNotNull(f.getName());
                assertTrue(f.getPluginCount() >= 0);
            }
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }

//    @Test
//    public void testGetFamily(){
//        System.out.println("getFamily");
//        try{
//            PluginFamily family = client.getFamily(familyId);
//            assertNotNull(family.getId());
//            assertNotNull(family.getName());
//            assertTrue(family.getPluginCount() >= 0);
//        } catch(NotFoundException ex){
//            fail(ex.getMessage());
//        } catch(ClientException ex){
//            fail(ex.getMessage());
//        } 
//    }
//    
}
