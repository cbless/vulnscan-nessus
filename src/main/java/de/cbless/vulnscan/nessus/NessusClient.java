/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.nessusclient.NessusClientBuilder;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.plugins.PluginManager;
import de.cbless.nessusclient.plugins.model.PluginDetailsResponse;
import de.cbless.nessusclient.plugins.model.resources.Family;
import de.cbless.nessusclient.scans.ScanManager;
import de.cbless.nessusclient.scans.model.HostDetailsResponse;
import de.cbless.nessusclient.scans.model.ScanDetailsResponse;
import de.cbless.nessusclient.scans.model.resources.Host;
import de.cbless.vulnscan.api.AbstractClient;
import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.NotFoundException;
import de.cbless.vulnscan.api.SessionException;
import de.cbless.vulnscan.api.model.Plugin;
import de.cbless.vulnscan.api.model.PluginFamily;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Christoph Bless
 */
public class NessusClient extends AbstractClient {

    private de.cbless.nessusclient.NessusClient client;
    private ScanManager scanManager;
    private PluginManager pluginManager;

    public NessusClient(String host, boolean trustAll) throws ClientException {
        this(host, 8834, trustAll);
    }    
    
    public NessusClient(String host, int port, boolean trustAll) 
            throws ClientException {
        super(host, port, trustAll);
        try{
            StringBuilder url = new StringBuilder();
            url.append("https://");
            url.append(host);
            url.append(":");
            url.append(port);
            NessusClientBuilder builder = new NessusClientBuilder(
                    url.toString(), trustAll);
            client = builder.build();
            scanManager = new ScanManager(client);
            pluginManager = new PluginManager(client);
        } catch (NoSuchAlgorithmException ex) {
            throw new ClientException(ex);
        } catch (KeyManagementException ex) {
            throw new ClientException(ex);
        } catch(NessusException ex){
            throw new ClientException(ex);
        } 
    }

    
    @Override
    public void login(String username, String password) 
            throws SessionException, ClientException {
        try{
            client.login(username, password);
        } catch(de.cbless.nessusclient.SessionException ex){
            throw new SessionException(ex);
        } catch (NessusException ex){
            throw new ClientException(ex);
        }
    }

    @Override
    public void logout() throws SessionException, ClientException {
        try{
            client.logout();
        } catch(de.cbless.nessusclient.SessionException ex) {
            throw new SessionException(ex);
        } catch(NessusException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public boolean isAuthenticated() {
        return client.isAuthenticated();
    }

    
    
    @Override
    public List<Scan> getScans() throws ClientException {
        try{
            List<de.cbless.nessusclient.scans.model.resources.Scan> scans = 
                    scanManager.list();
            List<Scan> result = new ArrayList<Scan>();
            for (de.cbless.nessusclient.scans.model.resources.Scan s : scans){
                Scan scan = NessusToAPI.toScan(s);
                result.add(scan);
            } 
            return result;
        } catch (NessusException ex ){
            throw new ClientException(ex);
        }
        
    }

    @Override
    public Scan getScan(String id) 
            throws NotFoundException, ClientException {
        int requestId = Integer.parseInt(id);
        try{
            de.cbless.nessusclient.scans.model.resources.Scan s = 
                    scanManager.getScan(requestId);
            return NessusToAPI.toScan(s);
        } catch (de.cbless.nessusclient.NotFoundException ex){
            throw new NotFoundException(ex);
        } catch (NessusException ex){
            throw new ClientException(ex);
        }                   
    }

    @Override
    public ScanDetails getScanDetails(String id) 
            throws ClientException, NotFoundException {
        try{
            int requestId = Integer.parseInt(id);
            ScanDetailsResponse response = scanManager.details(requestId);
            ScanDetails scan = NessusToAPI.toScanDetails(response);
            return scan;            
        } catch (NessusException ex){
            if (ex.getCause() instanceof javax.ws.rs.NotFoundException){
                throw new NotFoundException(ex);
            }
            throw new ClientException(ex);
        }
    }

    
    @Override
    public String getScanStatus(String id) throws ClientException, NotFoundException {
        Scan scan = getScan(id);
        return scan.getStatus();
    }
    
    @Override
    public String launch(String id) throws ClientException {
        try{
            scanManager.launch(Integer.parseInt(id));
            // return the scan id because this should be used to request a 
            // report
            return id;
        } catch(NessusException ex){
            throw new ClientException(ex);
        }
    }

    @Override
    public void pause(String id) throws ClientException {
        try{
            scanManager.pause(Integer.parseInt(id));
        } catch(NessusException ex){
            throw new NessusException(ex);
        }
    }

    @Override
    public String resume(String id) throws ClientException {
        try{
            scanManager.resume(Integer.parseInt(id));
            // return the scan id because this should be used to request the 
            // results of the scan
            return id;
        } catch(NessusException ex){
            throw new NessusException(ex);
        }
    }

    @Override
    public void stop(String id) throws ClientException {
        try{
            scanManager.stop(Integer.parseInt(id));
        } catch(NessusException ex){
            throw new NessusException(ex);
        }   
    }

    

    @Override
    public List<Vulnerability> getResults(String id) throws ClientException {
        int scanId = Integer.parseInt(id);
        try{
            ScanDetailsResponse details = scanManager.details(scanId);
            List<Vulnerability> vulns = new ArrayList<Vulnerability>();
            for (de.cbless.nessusclient.scans.model.resources.Vulnerability vuln : 
                    details.getVulnerabilities()){
                vulns.add(NessusToAPI.toVulnerability(vuln));
            }
            return vulns;
        } catch(NessusException ex){
            throw new ClientException(ex);
        }
        
    }

    @Override
    public Map<String, List<Vulnerability>> getResultsByHost(String id) throws ClientException {
        int scanId = Integer.parseInt(id);
        try{
            ScanDetailsResponse details = scanManager.details(scanId);
            Map<String, List<Vulnerability>> results = new HashMap<String , List<Vulnerability>>();
            for (Host h : details.getHosts()){
                HostDetailsResponse response = scanManager.hostDetails(scanId, 
                        h.getHostID());
                List<Vulnerability> vulns = new ArrayList<Vulnerability>();
                for (de.cbless.nessusclient.scans.model.resources.HostVulnerabilty v : 
                        response.getVulnerabilities()){
                    vulns.add(NessusToAPI.toVulnerability(v));
                }
                results.put(response.getInfo().getIp(), vulns);
            }
            return results;
        } catch(NessusException ex){
            throw new ClientException(ex);
        }
        
    }

    @Override
    public List<PluginFamily> listFamilies() throws ClientException{
        List<PluginFamily> families = new ArrayList<PluginFamily>();
        try{
            for (Family f : pluginManager.listFamilies()){
                families.add(NessusToAPI.toPluginFamily(f));
            }
        } catch(NessusException ex){
            throw new ClientException(ex);
        }
        return families;
    }

    @Override
    public Plugin getPlugin(String id) 
            throws NotFoundException, ClientException{
        try{
            int pluginId = Integer.parseInt(id);
            PluginDetailsResponse response = pluginManager.getDetails(pluginId);
            return NessusToAPI.toPlugin(response);
        } catch(NessusException ex){
            throw new ClientException(ex);
        }
    }

    
}
