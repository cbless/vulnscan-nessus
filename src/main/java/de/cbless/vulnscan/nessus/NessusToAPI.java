/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.nessus;

import de.cbless.nessusclient.plugins.model.FamilyDetailsResponse;
import de.cbless.nessusclient.plugins.model.PluginDetailsResponse;
import de.cbless.nessusclient.plugins.model.resources.Attribute;
import de.cbless.nessusclient.plugins.model.resources.Family;
import de.cbless.nessusclient.scans.model.ScanDetailsResponse;
import de.cbless.vulnscan.api.model.Plugin;
import de.cbless.vulnscan.api.model.PluginFamily;
import de.cbless.vulnscan.api.model.Reference;
import de.cbless.vulnscan.api.model.ReferenceType;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides some methods that can be used to convert instances of  
 * Nessusclient-types to instances of VulnScan-API types.
 * 
 * @author Christoph Bless
 */
public class NessusToAPI {
    
    /**
     * This method converts the scan-type from the Nessusclient to the 
     * scan-type from the VulnScan-API.
     * 
     * @param s Instance of a Nessusclient scan-type
     * @return  Instance of a VulnScan-API scan-type
     */
    public static Scan toScan(de.cbless.nessusclient.scans.model.resources.Scan s){
        Scan scan = new Scan();
        scan.setId(String.valueOf(s.getId()));
        scan.setName(s.getName());
        scan.setStatus(s.getStatus());
        return scan;
    }
    
    /**
     * This method converts a ScanDetailsResponse into an instance of the 
     * ScanDetails type from the VulnScan-API.
     * 
     * @param response Instance of a ScanDetailsResponse received from the 
     * Nessus server
     * @return Instance of a ScanDetails type from the VulnScan-API
     */
    public static ScanDetails toScanDetails(ScanDetailsResponse response){
        ScanDetails scan = new ScanDetails();
        scan.setId(String.valueOf(response.getInfo().getObjectId()));
        scan.setName(response.getInfo().getName());
        scan.setStatus(response.getInfo().getStatus());
        scan.setTargets(response.getInfo().getTargets());
        return scan;
    }
    
    /**
     * This method converts an instance of a Vulnerability class from the nessusclient 
     * into an instance of the Vulnerablity class from the VulnScan-API.
     * 
     * @param v Instance of the Vulnerability class from the nessusclient
     * @return Instance of the Vulnerability class from the VulnScan-API
     */
    public static Vulnerability toVulnerability(
            de.cbless.nessusclient.scans.model.resources.Vulnerability v)
    {
        Vulnerability vuln = new Vulnerability();
        vuln.setId(String.valueOf(v.getPluginId()));
        vuln.setName(v.getPluginName());
        vuln.setPluginId(String.valueOf(v.getPluginId()));
        vuln.setSeverity(toSeverity(v.getSeverity()));
        return vuln;
    }
    
    /**
     * This method converts an instance of a HostVulnerability class from the nessusclient 
     * into an instance of the Vulnerablity class from the VulnScan-API.
     * 
     * @param v Instance of the HostVulnerability class from the nessusclient
     * @return Instance of the Vulnerability class from the VulnScan-API
     */
    public static Vulnerability toVulnerability(
            de.cbless.nessusclient.scans.model.resources.HostVulnerabilty v){
        Vulnerability vuln = new Vulnerability();
        vuln.setId(String.valueOf(v.getPluginId()));
        vuln.setName(v.getPluginName());
        vuln.setPluginId(String.valueOf(v.getPluginId()));
        vuln.setSeverity(toSeverity(v.getSeverityIndex()));
        return vuln;
    }
    
    /**
     * This method converts an instance of a Family class from the nessusclient 
     * into an instance of the PluginFamily class from the VulnScan-API.
     * 
     * @param f Instance of the Family class from the nessusclient
     * @return Instance of the Vulnerability class from the VulnScan-API
     */
    public static PluginFamily toPluginFamily(Family f){
        PluginFamily family = new PluginFamily();
        family.setId(String.valueOf(f.getId()));
        family.setName(f.getName());
        family.setPluginCount(f.getCount());
        return family;
    }
    
    /**
     * This method converts an instance of a FamilyDetailsResponse to an 
     * instance of the PluginFamily class from the VulnScan-API.
     * 
     * @param r Instance of the FamilyDetailsResponse class from the nessusclient
     * @return Instance of the PluginFamily class from the VulnScan-API
     */
    public static PluginFamily toPluginFamily(FamilyDetailsResponse r){
        PluginFamily family = new PluginFamily();
        family.setId(String.valueOf(r.getId()));
        family.setName(r.getName());
        family.setPluginCount(r.getPlugins().size());
        return family;
    }
    
    
    /**
     * This method creates a Reference from a ReferenceType and a String 
     * reference. 
     * 
     * @param type Type of the Reference
     * @param ref Reference as String
     * @return Instance of the Reference class from the VulnScan-API
     */
    public static Reference toReference(ReferenceType type, String ref){
        Reference reference = new Reference();
        reference.setType(type);
        reference.setReference(ref);
        return reference;
    }
    
    /**
     * Creates a list of references from a list of attributes. 
     * 
     * @param attributes list of Attributes
     * @return list of References
     */
    public static List<Reference> toReferences(List<Attribute> attributes){
        List<Reference> references = new ArrayList<Reference>();
        if (attributes != null){
            for (Attribute a : attributes){
                if ("see also".equals(a.getName())){
                    references.add(toReference(ReferenceType.OTHER, a.getValue()));
                } else if("cve".equals(a.getName())){
                    references.add(toReference(ReferenceType.CVE, a.getValue()));
                } else if("bid".equals(a.getName())){
                    references.add(toReference(ReferenceType.BID, a.getValue()));
                } else if("xref".equals(a.getName())){
                    references.add(toReference(ReferenceType.XREF, a.getValue()));
                } else if("osvdb".equals(a.getName())){
                    references.add(toReference(ReferenceType.OSVDB, a.getValue()));
                } else if("cwe".equals(a.getName())){
                    references.add(toReference(ReferenceType.XREF, "CWE:"+a.getValue()));
                }
            }
        }
        return references;
    }
    
    /**
     * Converts a PluginDetailsResponse from the nessusclient to an instance of 
     * the Plugin class from the VulnScan-API
     * @param r Instance of the PluginDetailsResponse class from the nessusclient
     * @return Instance of the Plugin class from the VulnScan-API
     */
    public static Plugin toPlugin(PluginDetailsResponse r){
        System.out.println(r);
        Plugin plugin = new Plugin();
        plugin.setId(String.valueOf(r.getId()));
        plugin.setName(r.getName());
        List<Reference> refs = toReferences(r.getAttributes());
        plugin.setReferences(refs);
        plugin.setFamily(r.getFamilyName());
        if (r.getAttributes() != null){
            for (Attribute a : r.getAttributes()){
                if ("cvss_base_score".equals(a.getName())){
                    plugin.setCvss(a.getValue());
                } else if ("description".equals(a.getName())){
                    plugin.setDescription(a.getValue());
                } 
                // soloution
                // risk_factor
                // script_version
                // plugin_name
                // fname
                // plugin_type
                // plugin_modification_date
                // plugin_publication_date
                // synopsis
                // cpe
                // vuln_publicication_date
                // exploitability_ease
                // exploit_available


            }
        }
        if (plugin.getDescription()== null){
            plugin.setDescription("");
        }
        return plugin;
    }
    
    /**
     * Converts the given integer to a severity class as String.
     * @param i severity class as integer
     * @return severity class as String
     */
    public static String toSeverity(int i){
        // https://discussions.tenable.com/thread/8303
        switch(i){
            case 0:
                return "Info";
            case 1: 
                return "Low";
            case 2:
                return "Medium";
            case 3: 
                return "High";
            case 4:
                return "Critical";
            default:
                //throw new IllegalArgumentException("unknown severitylevel");
                return String.valueOf(i);
        }
    }
}
